#!/usr/local/bin/bash

# exit on any error
set -e

# set vars
PROF_FILE=~/.bash_profile
PRJPYVERSION=3.9.2
PRJPYENVROOT=~/.pyenv

# gcc shipped in OpenBSD base is too old.
# Override it with the one from packages.
export CC=/usr/local/bin/egcc
echo 'export CC=/usr/local/bin/egcc' >> $PROF_FILE

# install pyenv and pyenv-virtualenv
git clone https://github.com/pyenv/pyenv.git $PRJPYENVROOT \
 && git clone https://github.com/pyenv/pyenv-virtualenv.git $PRJPYENVROOT/plugins/pyenv-virtualenv \
 && cd $PRJPYENVROOT && src/configure && make -C src
echo "export PYENV_ROOT=\"$PRJPYENVROOT\"" >> $PROF_FILE
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> $PROF_FILE 
echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> $PROF_FILE 

# load the new profile to enable pyenv
source $PROF_FILE 

# install python and set up a virtualenv
pyenv install $PRJPYVERSION \
 && pyenv global $PRJPYVERSION \
 && pip install --upgrade pip \
 && pyenv virtualenv $PRJPYVERSION venv \
 && pyenv activate venv \
 && pip install --upgrade pip setuptools \
 && pyenv deactivate
