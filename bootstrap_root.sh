#!/bin/bash

# parse arguments
# format is --namedparam=value -boolparam
for argument in "$@"
do
    key=$(echo $argument | cut -f1 -d=)
    value=$(echo $argument | cut -f2 -d=)
    if [[ $key == *"--"* ]]; then
        v="${key/--/}"
        declare ${v}="${value}"
    elif [[ $key == *"-"* ]]; then
        v="${key/-/}"
        declare ${v}=true
   fi
done


# enable package management and add essentials
# uncomment next line to override default installurl
#echo "https://cdn.openbsd.org/pub/OpenBSD" > /etc/installurl
pkg_add git
# essentials to build python correctly
pkg_add sqlite3 libffi gcc readline
cp /etc/examples/doas.conf /etc/
echo "permit persist :wheel" >> /etc/doas.conf

# replace vi with vim
echo "alias vi=/usr/local/bin/vim" >> /root/.profile

# create superuser if option is passed
if [[ -v su ]]; then
	useradd -m -c "${su}" -L default -s /usr/local/bin/bash -p "changem3" $su
	usermod -G wheel $su
	echo "alias vi=/usr/local/bin/vim" >> /home/$su/.profile
fi

# config ssh
# note: file should be copied in Vagrantfile
if [[ -v ssh_pubkey ]]; then
	if [[ -v su ]]; then
		cat $ssh_pubkey >> /home/$su/.ssh/authorized_keys
	fi
	cat $ssh_pubkey >> /home/vagrant/.ssh/authorized_keys
	rm -f $ssh_pubkey
fi

# hook to install project-specific packages
pkg_add -I -l /app/obsd_packages.txt